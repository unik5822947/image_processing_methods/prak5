import os
os.environ['USE_PYGEOS'] = '0'
import pandas as pd
import geopandas as gpd
import numpy as np
import rasterio as rio
import matplotlib.pyplot as plt
import folium
import glob
import cv2

regions = gpd.read_file('E:/obrabotka_img/prak5/data/DeSO_region.gpkg')
cities = gpd.read_file('E:/obrabotka_img/prak5/data/cities.geojson')
vastra = gpd.read_file('E:/obrabotka_img/prak5/data/vastra.shp')
cities_proj = cities.to_crs({'init': 'epsg:3006'})
fig, ax  = plt.subplots(figsize=(14,10))
ax.set_aspect('equal')
#print(regions.head(5))
#print(regions.describe())
#print(regions.shape)
# fig, ax  = plt.subplots(figsize=(14,10))
# ax.set_aspect('equal')
# regions.plot(ax=ax, color='white', edgecolor='black')
# plt.show()
#print(regions.crs)

#Задание 2.1
# print(cities.head(5))
# print(cities.describe())
# print(cities.shape)
# cities.plot()
# print(cities.crs)

vastra_cities = gpd.sjoin(cities_proj, vastra, how="inner", op='within')
vastra_cities.plot()
# regions.plot(ax=ax, color='white', edgecolor='black')
# cities_proj.plot(ax=ax, marker = '*', color='red', markersize=5)
plt.show()